<?php

namespace Illuminate;

use Illuminate\Exceptions\InvalidArgumentException;

trait Validation
{
    /**
     * Validate the input
     * 
     */
    public function validate($input, $validation)
    {
        foreach ($validation as $field => $rules) {

            $rules = explode('|', $rules);

            foreach ($rules as $rule) {

                if (!$this->rule($rule, $field, $input)) {
                    return $this->exception($rule, $field, $input);
                }
            }
        }
    }

    /**
     * Make the rules
     * 
     */
    public function rule($rule, $field, $input)
    {
        if ($rule == 'required') {
            return array_key_exists($field, $input) && !empty($input[$field]);
        }
    }

    /**
     * Return the error exception
     * 
     */
    public function exception($rule, $field, $input)
    {
        if (method_exists($this, 'error')) {
            call_user_func([$this, 'error'], $rule, $field, $input);
        } else {
            new InvalidArgumentException("$field [$rule]");
        }
    }
}
