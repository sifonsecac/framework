<?php

namespace Illuminate;

use Illuminate\Contracts\CustomPostType as CustomPostTypeContract;

abstract class CustomPostType extends PostType implements CustomPostTypeContract
{	
    /**
     * 
     * 
     */
	const IS_PUBLIC = true;

    /**
     * 
     * 
     */
	const PUBLICLY_QUERYABLE = true;

    /**
     * 
     * 
     */
	const SHOW_UI = true;

    /**
     * 
     * 
     */
	const SHOW_IN_MENU = true;

    /**
     * 
     * 
     */
	const QUERY_VAR = true;

    /**
     * 
     * 
     */
	const CAPABILITY_TYPE = 'post';

    /**
     * 
     * 
     */
	const HAS_ARCHIVE = true;

    /**
     * 
     * 
     */
	const HIERACHICAL = false;

    /**
     * 
     * 
     */
	const MENU_POSITION = 6;

    /**
     * 
     * 
     */
	protected static $supports = ['title', 'editor'];

    /**
     * 
     * 
     */
	protected static $taxonomies = [];

    /**
     * 
     * 
     */
	protected static $menu_icon = '';

    /**
     * 
     * 
     */
	public static function registerPostype()
	{
        $labels = static::getLabels();

    	$args = [
            'labels'             => $labels,
    		'public'             => static::IS_PUBLIC,
    		'publicly_queryable' => static::PUBLICLY_QUERYABLE,
    		'show_ui'            => static::SHOW_UI,
    		'show_in_menu'       => static::SHOW_IN_MENU,
    		'query_var'          => static::QUERY_VAR,
    		'rewrite'            => [
                'slug' => static::getSlug()
            ],
    		'capability_type'    => static::CAPABILITY_TYPE,
    		'has_archive'        => static::HAS_ARCHIVE,
    		'hierarchical'       => static::HIERACHICAL,
    		'menu_position'      => static::MENU_POSITION,
			'supports'           => static::$supports,
			'taxonomies'		 => static::$taxonomies,
			'menu_icon'			 => static::$menu_icon
        ];

    	static::registerPostypeHook($args);
    }

    /**
     * 
     * 
     */
    public static function classname()
    {
        $classname = get_called_class();

        $shortname = basename(str_replace('\\', '/',  $classname));

        return snake($shortname);
    }

    /**
     * 
     * 
     */
	public final static function registerPostypeHook(array $args)
	{
        register_post_type(static::classname(), $args);
    }

}
