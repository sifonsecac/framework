<?php

namespace Illuminate;

use Illuminate\Contracts\Controller as ControllerContract;
use ReflectionClass;

abstract class Controller implements ControllerContract
{
    use Validation;

    public function handle()
    {
        unset($_POST['action']);

        return $this->store($_POST);
    }

    public function error($url)
    {
        if ($url) {
            wp_safe_redirect($url);
        } elseif (wp_get_referer()) {
            wp_safe_redirect(wp_get_referer());
        } else {
            wp_safe_redirect(url());
        }
    }

    public function success($url)
    {
        wp_safe_redirect($url);
    }

    public function registerController()
    {
        $name = $this->classname();

        add_action('admin_post_nopriv_' .  $name, [$this, 'handle']);
        add_action('admin_post_' . $name, [$this, 'handle']);
    }

    public function classname()
    {
        return snake(str_replace('Controller', '', (new ReflectionClass($this))->getShortName()));
    }
}
