<?php

function asset($path)
{
    return template_url('public/' . $path);
}

function template_url($path = '')
{
    return get_bloginfo('template_url') . '/' . $path;
}

function url($path = '')
{
    return get_site_url() . '/' . $path;
}

function base_path($path = '')
{
    return get_stylesheet_directory() . '/' . $path;
}

function snake($input)
{
	return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
}