<?php

namespace Illuminate;

use Illuminate\Contracts\Ajax as AjaxContract;
use ReflectionClass;

abstract class Ajax implements AjaxContract
{
	use Validation;

	public function handle()
	{
		unset($_POST['action']);

		return $this->store($_POST);
	}

	public function error($msg)
	{
		// Set error code
		header('Content-Type: application/json');
		http_response_code(422);

		echo json_encode([
			'message' => $msg,
			'success' => false
		]);
		die;
	}

	public function success(array $data = [])
	{
		header('Content-Type: application/json');
		http_response_code(200);

		echo json_encode(array_merge($data, ['success' => true]));
		die;
	}

	public function registerAjax()
	{
		$name = $this->classname();

		add_action('wp_ajax_nopriv_' .  $name, [$this, 'handle']);
		add_action('wp_ajax_' . $name, [$this, 'handle']);
	}

	public function classname()
	{
		return snake(str_replace('Ajax', '', (new ReflectionClass($this))->getShortName()));
	}
}
