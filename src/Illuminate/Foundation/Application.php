<?php 

namespace Illuminate\Foundation;

class Application
{
    /**
     * 
     * 
     */
    public $path;

    /**
     * 
     * 
     */
    public $config;

    /**
     * 
     * 
     */
    public function __construct($path)
    {
        // Theme path where application will start
        $this->path = $path;

        // Load de theme config file. 
        $this->config = require $this->path.'/config/app.php';
        
        // We define all required constants before loading providers.
        define( 'BLOGURL', get_site_url() );
        define( 'THEMEURL', get_bloginfo('template_url') . '/' );
        define( 'TRANSDOMAIN', wp_get_theme()->template );

        foreach($this->config['providers'] as $provider) {
            $object = new $provider($this);
            $object->boot();
        }
    }
}