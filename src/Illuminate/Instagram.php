<?php 

namespace Illuminate;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Instagram
{
    /**
     * The url to make requests
     * 
     * @var string
     */
    protected static $url = "https://graph.instagram.com/";

    /**
     * A HTTP client for making requests
     *  
     * @var GuzzleHttp\Client
     */
    protected $http;

    /**
     * 
     * 
     */
    public function __construct()
    {
        $this->http = new Client([
            'base_uri' => self::$url
        ]);
    }
    
    /**
     * Return the recent media of the access token profile
     * 
     * @return array
     */
    public function getMyMedia($fields = [])
    {
        if (count($fields) == 0) {
            $fields = ['media_url', 'permalink', 'media_type', 'id'];
        }

        try {
            $response = $this->http->get('/me/media/', [
                'query' => [
                    'access_token' => self::getAccessToken(),
                    'fields'       => implode(',', $fields),
                ]
            ]);
        } catch (RequestException $e) {
            return [];
        }

        $body = $response->getBody();

        return json_decode($body->getContents());
    }
    
    /**
     * 
     * 
     */
    public static function getAccessToken()
    {
        if(!defined('INSTAGRAM_ACCESS_TOKEN')){
            throw new Exception('No instagram access token provided.');
        }
        
        return INSTAGRAM_ACCESS_TOKEN;
    }
}
