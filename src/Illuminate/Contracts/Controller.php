<?php

namespace Illuminate\Contracts;

interface Controller
{
    public function handle();

    public function store($input);

    public function error($url);
    
    public function success($url);

    public function registerController();

    public function classname();
}
