<?php

namespace Illuminate\Contracts;

interface Ajax
{
	public function handle();

    public function store($input);

    public function error($msg);
    
    public function success(array $data = []);

    public function registerAjax();

    public function classname();
}
